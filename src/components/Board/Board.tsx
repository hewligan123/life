import * as React from 'react';

import { gameState, gameRowState } from '../../lib/game/gameTypes';

import { Row } from '../Row/Row';

import BoardCss from './Board.css';

interface BoardProps {
    gameBoard: gameState;
}

export const Board = (props:BoardProps) => (
    <div className={BoardCss.board}>
        {props.gameBoard.map((row:gameRowState, i:number) => <Row key={i} rowState={row} />)}
    </div>
);
