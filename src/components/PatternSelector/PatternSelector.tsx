import * as React from 'react';
import { gameStateSummaryList } from '../../lib/game/gameTypes';

import PatternSelectorCss from './PatternSelector.css';

interface Props {
    stateList: gameStateSummaryList;
    setPattern: Function;
}

export class PatternSelector extends React.Component<Props> {
    constructor(props:Props) {
        super(props);

        this.setPattern = this.setPattern.bind(this);
    }

    setPattern(e:React.SyntheticEvent) {
        this.props.setPattern((e.currentTarget as HTMLInputElement).value);
    }

    render() {
        return (
            <div>
                <select className={PatternSelectorCss.selector} onChange={this.setPattern}>
                    {
                        this.props.stateList.map(description => (
                            <option value={description._id} key={description._id}>
                                {description.name}
                            </option>
                        ))
                    }
                </select>
            </div>
        );
    }
}
