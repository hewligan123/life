import * as React from 'react';
import { gameRowState, gameCellState } from '../../lib/game/gameTypes';
import { Cell } from '../Cell/Cell';

import styles from './Row.css';

interface RowProps {
    rowState: gameRowState
}

export const Row = (props:RowProps) => (
    <div className={styles.row}>
        {props.rowState.map((cell:gameCellState, i:number) => (<Cell key={i} cellState={cell} />))}
    </div>
);
