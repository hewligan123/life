import * as React from 'react';
import { render } from 'react-dom';

import { Page } from './components/Page/Page';

export const init = () => {
    const appRoot = document.getElementById('app-root');

    render(
        <Page />,
        appRoot,
    );
};
