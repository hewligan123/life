import { gameState, gameRowState, gameCellState } from './gameTypes';

const A = true;
const D = false;

const getNextPosition = (length:number, position:number) => position > (length - 2) ? 0 : (position + 1);
const getPreviousPosition = (length:number, position:number) => position < 1 ? length - 1 : (position - 1);

const getCellState = (boardState:gameState, row:number, col:number) => {
    const isAlive = boardState[row][col];
    const prevRowIndex = getPreviousPosition(boardState.length, row);
    const nextRowIndex = getNextPosition(boardState.length, row);
    const prevColIndex = getPreviousPosition(boardState[0].length, col);
    const nextColIndex = getNextPosition(boardState[0].length, col);

    const prevRow = boardState[prevRowIndex];
    const currentRow = boardState[row];
    const nextRow = boardState[nextRowIndex]

    const surroundingScore = (
        Number(prevRow[prevColIndex]) + Number(prevRow[col]) + Number(prevRow[nextColIndex])
        + Number(currentRow[prevColIndex]) + Number(currentRow[nextColIndex])
        + Number(nextRow[prevColIndex]) + Number(nextRow[col]) + Number(nextRow[nextColIndex])
    )

    /* if (row == 8 && (col == 9)) {
        console.log(row, col, prevColIndex, nextColIndex, boardState[row][col], surroundingScore);
        //console.table(boardState);
    } */

    if (isAlive && surroundingScore > 1 && surroundingScore < 4) {
        return A;
    }
    
    if (!isAlive && surroundingScore === 3){
        return A;
    }

    return D;
}

export const getNextState = (currentState:gameState) => (
    currentState.map(
        (row:gameRowState, rowNumber:number) => row.map(
            (cell: gameCellState, cellNumber:number) => getCellState(currentState, rowNumber, cellNumber)
        )
    )
);
