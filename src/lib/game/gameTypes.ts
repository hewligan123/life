
export type gameCellState = boolean;
export type gameRowState = gameCellState[];
export type gameState = gameRowState[];

export type gameStateDescription = {
    name: string,
    _id: string,
    width: number,
    height: number,
    initialState: gameState,
};

export type gameStateSummary = {
    name: string,
    _id: string,
};

export type gameStateSummaryList = gameStateSummary[];

export type gameStateDescriptionMap = {
    [x:string]: gameStateDescription,
};

export type gameStateDescriptionList = gameStateDescription[];
