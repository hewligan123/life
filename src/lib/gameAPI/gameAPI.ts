import axios, { AxiosPromise } from 'axios';

import settings from '../../settings';

import { gameStateSummaryList, gameStateDescription } from '../game/gameTypes';

const GAME_API_URL = `${settings.server}${settings.gameStartRoot}`;

export const getGameSummaries = async (page:number):Promise<gameStateSummaryList> => {
    const response = await axios.get(`${GAME_API_URL}?page=${page}`);
    return response.data;
};

export const getGameState = async (id:string):Promise<gameStateDescription> => {
    const response = await axios.get(`${GAME_API_URL}/${id}`);
    return response.data;
};
