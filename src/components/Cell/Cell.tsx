import * as React from 'react';

import classNames from 'classnames';

import { gameCellState } from '../../lib/game/gameTypes';

import styles from './Cell.css';

interface CellProps {
    cellState: gameCellState
}


export const Cell = (props:CellProps) =>  (
    <div className={classNames([
        styles.cell,
        {
            [styles.alive]: props.cellState,
            [styles.dead]: !props.cellState
        }
    ])} />
);
