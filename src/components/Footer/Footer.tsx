import * as React from 'react';

import FooterCss from './Footer.css';

interface FooterProps {
    children: React.ReactNode;
}

export const Footer = (props:FooterProps) => (
    <footer className={FooterCss.pageFooter}>
        {props.children}
    </footer>
);
