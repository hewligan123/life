import * as React from 'react';
import { getNextState } from '../../lib/game/game';
import {
    gameState,
    gameStateDescription,
    gameStateDescriptionList,
    gameStateSummaryList,
} from '../../lib/game/gameTypes';
import { getGameState, getGameSummaries } from '../../lib/gameAPI/gameAPI';

import { Header } from '../Header/Header';
import { Board } from '../Board/Board';
import { PatternSelector } from '../PatternSelector/PatternSelector';
import { Footer } from '../Footer/Footer';
import { Button } from '../Button/Button';

import './fonts.css';
import './base.css';
import PageCss from './Page.css';

interface PageState {
    gameBoard: gameState;
    pattern: gameStateDescription;
    interval: number;
    gameDescriptions: gameStateSummaryList;
    loadingDescriptions: boolean;
    loadingBoard: boolean;
}

export class Page extends React.Component<{}, PageState> {
    constructor(props:{}) {
        super(props);
        this.state = {
            gameBoard: null,
            pattern: null,
            interval: null,
            gameDescriptions: [],
            loadingDescriptions: true,
            loadingBoard: true,
        };
        this.setPattern = this.setPattern.bind(this);
        this.toggle = this.toggle.bind(this);
        this.step = this.step.bind(this);
        this.reset = this.reset.bind(this);
    }

    componentDidMount() {
        this.loadGameData();
    }

    async getGameSummaries(page = 0) {
        return await getGameSummaries(page);
    }

    async getGameBoard(id:string) {
        return await getGameState(id);
    }

    async setPattern(id:string) {
        this.setState({ loadingBoard: true });
        const pattern = await getGameState(id);
        this.setState({
            pattern,
            gameBoard: pattern.initialState,
            loadingBoard: false,
        });
    }

    async loadGameData() {
        const gameDescriptions = await this.getGameSummaries();
        this.setState({
            gameDescriptions,
            loadingDescriptions: false,
        });
        console.log(gameDescriptions);

        if (gameDescriptions.length) {
            this.setPattern(gameDescriptions[0]._id);
        }
    }

    updateBoardState() {
        this.setState({
            gameBoard: getNextState(this.state.gameBoard),
        });
    }

    start() {
        this.setState({
            interval: window.setInterval(this.updateBoardState.bind(this), 500),
        });
    }

    stop() {
        this.state.interval && window.clearInterval(this.state.interval);
        this.setState({
            interval: null,
        });
    }

    step() {
        this.updateBoardState();
    }

    toggle() {
        if (this.state.interval) {
            this.stop();
        } else {
            this.start();
        }
    }

    reset() {
        this.setState({
            gameBoard: this.state.pattern.initialState,
        });
    }

    render() {
        return (
            <div>
                <Header />
                <div className={PageCss.boardContainer}>
                    {!this.state.loadingBoard &&  <Board gameBoard={this.state.gameBoard} />}
                </div>
                <Footer>
                        <div className={PageCss.controlsContainer}>
                        <div className={PageCss.btnContainer}>
                            <Button onClick={this.toggle}>
                                {this.state.interval ? 'Stop' : 'Start'}
                            </Button>
                            <Button onClick={this.step}>Step</Button>
                            <Button onClick={this.reset}>Reset</Button>
                        </div>
                        <div className={PageCss.selectorContainer} >
                            <PatternSelector
                                setPattern={this.setPattern}
                                stateList={this.state.gameDescriptions}
                            />
                        </div>
                    </div>
                </Footer>
            </div>
        );
    }
}
