import * as React from 'react';

import styles from './Header.css';

export const Header = () => (
    <header className={styles.pageHeader}>
        <div>
            <h1 className={styles.pageTitle}>Life</h1>
        </div>
    </header>
);