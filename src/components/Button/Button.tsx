import * as React from 'react';

import classnames from 'classnames';

import ButtonCss from './Button.css';

type ButtonProps = {
    className?: string,
    [propName: string]: any;
};

export const Button = (props:ButtonProps) => {
    const { className, ...remainingProps } = props;

    return (
        <button className={classnames([ButtonCss.btn, className])} {...remainingProps} />
    );
};
